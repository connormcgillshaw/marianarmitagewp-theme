  <head>
    <meta charset="utf-8">
    <title>Marian Armitage</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    <link href="<?php bloginfo('template_url');?>/css/custom.css" rel="stylesheet">
    <link src="fonts/alexfont.css" rel="stylesheet">

	<!-- Scripts -->
    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
  </head>
  <body>

<div id="navbar" class="navbar navbar-fixed-top navbar-default">
  <div class="container">
    <div class="container"><div class="navbar-header"><a class="navbar-brand" href="<?php echo site_url(); ?>">Marian Armitage | Author</a></div>
	
    </div>
  </div>
</div>

